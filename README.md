# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

> * 功能說明(由左至右，由上至下介紹)
> * 粗體為spec沒有的功能

#### 選擇工具 Selectors

> 工具列最上排為四條Slider與兩個下拉式選單，分別控制：

* Brush Size: 筆刷大小
* Red, Green, Blue: 筆刷/文字的RGB三原色
* Font Type: 文字字型
* Font Size: 文字大小

#### 繪製工具 Paint Tools

* Pen: 筆刷
> * 每次mousemove時擷取滑鼠的座標，利用lineto和stroke畫線。
* Eraser: 橡皮擦
> * 同筆刷，但顏色固定為白色(#FFFFFF)。
* **Bucket**: 水桶
> * 用選定的顏色填滿畫面。
* Text: 文字
> * 點擊canvas時將生出一個文字框，可於其中輸入文字。
> * 若按下Enter或滑鼠點擊canvas其他地方，文字框會消失，並將輸入的文字印上。
> * 若按下Esc，文字框會消失，而且不將文字印上。

#### 圖形工具 Shapes

* Rectangle: 繪製矩形
> * 每次mousemove時先利用loadLastState這個function變回最新儲存的畫面，再根據mousedown的座標和現在的座標繪製矩形。
> * 此處使用promise的概念，確保load完原本的畫面後，才會將圖形畫上去。
* Triangle: 繪製三角形
> * 概念同矩形。
* Circle: 繪製圓形
> * 概念同矩形。
* **Line**: 繪製直線
> * 概念同矩形。

#### 額外工具 Additional Tools

* **Rainbow**: 彩虹筆刷
> * 同筆刷，但顏色根據changeRainbowColor這個function控制RGB三原色。
> * 顏色變化：紅→橙→黃→綠→藍→靛→紫→紅。

#### 其他工具 Others

* Undo: 往回一步
> * 我在global設了一個cPushArray，用於儲存canvas各個時期的狀態。
> * 使用畫筆、圖形時以每次mouseup/mouseleave為單位，將現有狀態push進去。
> * 使用文字時則以文字框消失為單位，將現有狀態push進去。
> * Undo做的即是尋找cPushArray中index減1的圖形是否存在，若存在便回到該狀態。
* Redo: 往前一步
> * Redo做的則是尋找cPushArray中index加1的圖形是否存在，若存在便前往該狀態。
* Clear: 清空畫布
> * 此處我設計會連同push的紀錄一併清除。
> * 清空前會跳出alert警示。
* **Hollow/Solid**: 空心/實心
> * 點擊可使圖形工具由空心切換為實心，或反之。
> * Button icon也會變(第一次變有時候會卡卡的)。
* Download 下載圖片
> * 可將canvas上現有的畫面下載。
* Upload 上傳圖片
> * 可從local端選擇圖片上傳。
> * 上傳圖片若超過canvas大小，將無法完整顯示。

#### Button Icon Credit
* https://www.flaticon.com/

#### Cursor Icon Credit
* 我