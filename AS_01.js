var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
var cPushArray = new Array();
var cStep = -1;
var tool = "none";
var begin_x = 0;
var begin_y = 0;
var isFilled = true;
var isDrawing = false;
var isTyping = false;
const toolNum = 9;
const shapeNum = 3;

function initFunction(){
    ctx.fillStyle = "#ffffff";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    _cPush();
    ctx.lineCap = 'round';
    ctx.lineJoin = 'round';
    changeTool("pen");
    changeBody();
    
    var header = document.getElementById("table");
    var btns = header.getElementsByClassName("btn");
    for (var i = 0; i < toolNum; i++) {
        btns[i].addEventListener("click", function() {
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            this.className += " active";
        });
    }
}

function changeTool(input_tool){
    if(isTyping){
        var textBox = document.getElementById("_TB");
        textBox.value = "";
        deleteTextBox();
    }

    tool = input_tool;
    if(input_tool === "text"){
        canvas.style.cursor = "text";
    } else {
        canvas.style.cursor = `url('./icon/${input_tool}.png'), auto`;
    }
    // switch(tool){
    //     case "pen": canvas.style.cursor = "url('./icon/pen.png'), auto"; break;
    //     case "eraser": canvas.style.cursor = "url('./icon/eraser.png'), auto"; break;
    //     case "rectangle": canvas.style.cursor = "url('./icon/rectangle.png'), auto"; break;
    //     case "circle": canvas.style.cursor = "url('./icon/circle.png'), auto"; break;
    //     case "triangle": canvas.style.cursor = "url('./icon/triangle.png'), auto"; break;
    //     case "line": canvas.style.cursor = "url('./icon/line.png'), auto"; break;
    //     case "bucket": canvas.style.cursor = "url('./icon/bucket.png'), auto"; break;
    //     case "rainbow": canvas.style.cursor = "url('./icon/rainbow.png'), auto"; break;
    //     case "text": canvas.style.cursor = "text"; break;
    //     default: canvas.style.cursor = "default";
    // }
}

function changeBody(){
    isFilled = !isFilled;
    var btn = document.getElementById("fill");
    btn.value = (isFilled) ? "Solid" : "Hollow";
    btn.style.backgroundColor = (isFilled) ? "#101010" : "#CCCCCC";
    btn.style.color = (isFilled) ? "#CCCCCC" : "#101010";

    var btns = document.getElementsByClassName("shape");
    for (var i = 0; i < shapeNum; i++) {
        btns[i].style.backgroundImage = (isFilled) ? `url("./icon/btn_${btns[i].id}_solid.png")` : `url("./icon/btn_${btns[i].id}.png")`;
    }
}

function changeColor(r, g, b){
    ctx.strokeStyle = "rgb("+r+","+g+","+b+")";
    ctx.fillStyle = "rgb("+r+","+g+","+b+")";
};

function getX(evt){
    return evt.offsetX;
}

function getY(evt){
    return evt.offsetY;
}

function createTextBox(x, y) {
    var textBox = document.createElement("input");
    textBox.id = "_TB";
    textBox.style.position = "absolute";
    textBox.style.left = `${x+9}px`;
    textBox.style.top = `${y}px`;
    document.getElementById("canvasDiv").appendChild(textBox); // Why can't on canvas?

    textBox.addEventListener("keydown", function(evt){
        if(evt.which == 13){
            /* If Press Enter Key */
            deleteTextBox();
            // _cPush();
            setTimeout(() => { _cPush(); }, 10);
        } else if(evt.which == 27){
            /* If Press Esc Key */
            textBox.value = "";
            deleteTextBox();
        } else {
            // console.log(":)");
        }
    });
}

function deleteTextBox() {
    var textBox = document.getElementById("_TB");
    isTyping = false;
    console.log("input: " + textBox.value);
    var r = document.getElementById("red").value;
    var g = document.getElementById("green").value;
    var b = document.getElementById("blue").value;
    changeColor(r, g, b);
    drawText(textBox.value);
    document.getElementById("canvasDiv").removeChild(textBox);
}

async function draw(evt){

    console.log("draw");

    var x = getX(evt);
    var y = getY(evt);
    
    switch(tool){
        case "pen":
            ctx.lineTo(x, y);
            ctx.stroke();
        break;
        case "eraser":
            ctx.lineTo(x, y);
            ctx.stroke();
        break;
        case "rectangle":
            await loadLastState();
            drawRect(x, y);
        break;
        case "circle":
            await loadLastState();
            drawCircle(x, y);
        break;
        case "triangle":
            await loadLastState();
            drawTriangle(x, y);
        break;
        case "line":
            await loadLastState();
            drawLine(x, y);
        break;
        case "rainbow":
            changeRainbowColor();
            ctx.lineTo(x, y);
            ctx.stroke();
            ctx.beginPath();
            ctx.moveTo(x, y);
        break;
    }
}

function loadLastState(){
    const promise = new Promise((res, rej) => {
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { 
            ctx.drawImage(canvasPic, 0, 0);
            res();
        }
    });
    return promise;
}

function drawRect(x, y){
    ctx.strokeRect(begin_x, begin_y, x-begin_x, y-begin_y);
    if(isFilled){
        ctx.fillRect(begin_x, begin_y, x-begin_x, y-begin_y);
    }
}

function drawCircle(x, y) {
    ctx.beginPath();
    ctx.arc((begin_x + x)/2, (begin_y + y)/2, Math.max(Math.abs((x-begin_x)/2), Math.abs((y-begin_y)/2)), 0, 2*Math.PI);
    ctx.stroke();
    if(isFilled){
        ctx.fill();
    }
}

function drawTriangle(x, y){
    ctx.beginPath();
    ctx.moveTo(x, Math.max(begin_y, y));
    ctx.lineTo(begin_x, Math.max(begin_y, y));
    ctx.lineTo((begin_x + x)/2, Math.min(begin_y, y));
    ctx.closePath();
    ctx.stroke();
    if(isFilled){
        ctx.fill();
    }
}

function drawLine(x, y){
    ctx.beginPath();
    ctx.moveTo(begin_x, begin_y);
    ctx.lineTo(x, y);
    // ctx.closePath();
    ctx.stroke();
}

function drawText(str) {
    var fontSize = document.getElementById("fontsize").value;
    var fontType = document.getElementById("fonttype").value;
    ctx.font = `${fontSize}px ${fontType}`;
    ctx.fillText(str, begin_x, begin_y);
}

// var el = document.getElementById("myCanvas");
canvas.addEventListener("mousedown", function(evt){

    var x = getX(evt);
    var y = getY(evt);

    if(!isDrawing && !isTyping){
        begin_x = x;
        begin_y = y;
    }

    if(tool === "text"){
        if(!isTyping){
            createTextBox(x, y);
            isTyping = true;
        } else if(isTyping) {
            deleteTextBox();
            setTimeout(() => { _cPush(); }, 10);
        }
    } else {
        isDrawing = true;
    }

    if(tool === "eraser"){
        changeColor(255, 255, 255);
    } else if(tool === "rainbow") {
        changeRainbowColor();
    } else {
        var r = document.getElementById("red").value;
        var g = document.getElementById("green").value;
        var b = document.getElementById("blue").value;
        changeColor(r, g, b);
    }

    var size = document.getElementById("size");
    ctx.lineWidth = size.value;

    if(tool === "pen" || tool === "eraser" || tool === "rainbow"){
        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(x, y);
        ctx.stroke();
    }

    if(tool === "bucket"){
        ctx.fillRect(0, 0, canvas.width, canvas.height);
    }

    if(isDrawing) canvas.addEventListener("mousemove", draw);
});

canvas.addEventListener("mouseup", function(){
    if(isDrawing){
        isDrawing = false;

        canvas.removeEventListener('mousemove', draw);
        setTimeout(() => { _cPush(); }, 10);
    }
});

canvas.addEventListener("mouseleave", function(){
    if(isDrawing){
        isDrawing = false;

        canvas.removeEventListener('mousemove', draw);
        setTimeout(() => { _cPush(); }, 10);
    }
});

function clearFunction(){
    var r = confirm("Clear all?");
    if (r == true) {
        console.log("clear");
        ctx.closePath();
        canvas.removeEventListener('mousemove', draw);
        ctx.fillStyle = "#ffffff";
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        cStep = 0;
        cPushArray.length = 1;
    } else {
        console.log("cancelled");
    }
}

var imageLoader = document.getElementById('imageLoader');
imageLoader.addEventListener('change', handleImage, false);

function handleImage(evt){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            // canvas.width = img.width;
            // canvas.height = img.height;
            ctx.drawImage(img,0,0);
            _cPush();
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(evt.target.files[0]);     
}

function _cPush() {
    
    console.log("push");

    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(canvas.toDataURL());
}

function cUndo() {
    if (cStep > 0) {

        console.log("undo");
    
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}

function cRedo() {
    if (cStep < cPushArray.length-1) {
    
        console.log("redo");
        
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}

var rainbow_r = 255;
var rainbow_g = 0;
var rainbow_b = 0;
var rainbow_state = 0;
const rainbow_change_speed = 5;

function changeRainbowColor(){
    // console.log(rainbow_state);
    switch(rainbow_state){
        case 0:
            rainbow_g += rainbow_change_speed;
            if(rainbow_g == 255){
                rainbow_state = 1;
            }
        break;
        case 1:
            rainbow_r -= rainbow_change_speed;
            if(rainbow_r == 0){
                rainbow_state = 2;
            }
        break;
        case 2:
            rainbow_g -= rainbow_change_speed;
            rainbow_b += rainbow_change_speed;
            if(rainbow_b == 255){
                rainbow_state = 3;
            }
        break;
        case 3:
            rainbow_g += rainbow_change_speed;
            if(rainbow_g == 255){
                rainbow_state = 4;
            }
        break;
        case 4:
            rainbow_r += rainbow_change_speed;
            rainbow_g -= rainbow_change_speed;
            if(rainbow_r == 255){
                rainbow_state = 5;
            }
        break;
        case 5:
            rainbow_b -= rainbow_change_speed;
            if(rainbow_b == 0){
                rainbow_state = 0;
            }
        break;
    }
    changeColor(rainbow_r, rainbow_g, rainbow_b);
}

function downloadImage(){
    var download = document.getElementById("download");
    var image = document.getElementById("myCanvas").toDataURL("image/png").replace("image/png", "image/octet-stream");
    download.setAttribute("href", image);
}

// #866ec7
// #bf9fee
// #ffd7e8